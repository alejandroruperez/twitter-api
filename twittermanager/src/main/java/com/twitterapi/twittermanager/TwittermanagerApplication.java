package com.twitterapi.twittermanager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;

import java.io.IOException;

@SpringBootApplication
public class TwittermanagerApplication {

	@Autowired
	private TwitterStream twitterStream;

	public static void main(String[] args) throws TwitterException, IOException {
		SpringApplication.run(TwittermanagerApplication.class, args);
	}
}
