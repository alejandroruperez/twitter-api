package com.twitterapi.twittermanager.repositories;

import com.twitterapi.twittermanager.model.HashtagInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface HashtagRepository  extends JpaRepository<HashtagInfo, Long> {
    Optional<HashtagInfo> findByName(String name);

    List<HashtagInfo> findTop10ByOrderByTimesDesc();
}
