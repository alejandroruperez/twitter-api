package com.twitterapi.twittermanager.repositories;

import com.twitterapi.twittermanager.model.TweetInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TweetInfoRepository extends JpaRepository<TweetInfo, Long> {
    List<TweetInfo> findByValidated(Boolean validated);
    Optional<TweetInfo> findById(Long id);

    void deleteById(Long id);
}
