package com.twitterapi.twittermanager.services;

import com.twitterapi.twittermanager.model.TweetInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import twitter4j.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class TwitterStatusListener implements StatusListener {

    //TODO @Value("${twitterConf.minFollowersNo}")
    private Integer minFollowersNo = 1500;

    @Autowired
    private TweetInfoService tweetInfoService;

    @Autowired
    private HashtagService hashtagService;

    public void onStatus(Status status) {
        int followersNo = status.getUser().getFollowersCount();

        if(followersNo < minFollowersNo) {
            return;
        }

        TweetInfo tweetInfo = composeTweetInfo(status);

        tweetInfoService.save(tweetInfo);
        saveHashtags(status);
    }

    private TweetInfo composeTweetInfo(Status status) {
        String location = Optional
                .ofNullable(status.getGeoLocation())
                .map(l -> l.getLatitude() + ", " + l.getLongitude())
                .orElse("<unknown>");

        TweetInfo tweetInfo = new TweetInfo();
        tweetInfo.setUserName(status.getUser().getName());
        tweetInfo.setText(status.getText());
        tweetInfo.setLocation(location);
        tweetInfo.setValidated(false);

        return tweetInfo;
    }

    private void saveHashtags(Status status) {
        List<String> hashtags = Arrays.stream(status.getHashtagEntities())
                .map(HashtagEntity::getText)
                .collect(Collectors.toList());

        hashtags.forEach(h -> hashtagService.saveHashtag(h));
    }

    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {}

    public void onTrackLimitationNotice(int numberOfLimitedStatuses) {}

    @Override
    public void onScrubGeo(long l, long l1) {}

    @Override
    public void onStallWarning(StallWarning stallWarning) {}

    public void onException(Exception ex) {
        ex.printStackTrace();
    }
}
