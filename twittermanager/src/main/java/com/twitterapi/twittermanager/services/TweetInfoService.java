package com.twitterapi.twittermanager.services;

import com.twitterapi.twittermanager.model.TweetInfo;
import com.twitterapi.twittermanager.repositories.TweetInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TweetInfoService {

    private TweetInfoRepository tweetInfoRepository;

    @Autowired
    public TweetInfoService(TweetInfoRepository tweetInfoRepository) {
        this.tweetInfoRepository = tweetInfoRepository;
    }

    public void save(TweetInfo tweetInfo) {
        tweetInfoRepository.save(tweetInfo);
    }

    public List<TweetInfo> findAllTweets() {
        return tweetInfoRepository.findAll();
    }

    public Optional<TweetInfo> findTweet(Long tweetId) {
        return tweetInfoRepository.findById(tweetId);
    }

    public List<TweetInfo> findValidatedTweets() {
        return tweetInfoRepository.findByValidated(true);
    }
}
