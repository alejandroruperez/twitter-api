package com.twitterapi.twittermanager.services;

import com.twitterapi.twittermanager.model.HashtagInfo;
import com.twitterapi.twittermanager.repositories.HashtagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class HashtagService {

    @Autowired
    private HashtagRepository hashtagRepository;

    public void saveHashtag(String hashtagName) {
        Optional<HashtagInfo> hashtagInfoOpt = hashtagRepository.findByName(hashtagName);
        HashtagInfo hashtagInfo;

        if(hashtagInfoOpt.isPresent()) {
            hashtagInfo = hashtagInfoOpt.get();
            Integer count = hashtagInfo.getTimes();
            hashtagInfo.setTimes(count + 1);
        } else {
            hashtagInfo = new HashtagInfo();
            hashtagInfo.setName(hashtagName);
            hashtagInfo.setTimes(1);
        }

        hashtagRepository.save(hashtagInfo);
    }

    public List<HashtagInfo> getTop10Hashtags() {
        return hashtagRepository.findTop10ByOrderByTimesDesc();
    }
}
