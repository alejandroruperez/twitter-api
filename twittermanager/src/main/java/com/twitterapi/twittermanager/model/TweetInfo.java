package com.twitterapi.twittermanager.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "tweets_info")
@Data
public class TweetInfo {
    @Id
    @GeneratedValue
    private Long id;

    private String userName;

    @Column(length = 600)
    private String text;

    private String location;
    private Boolean validated;
}
