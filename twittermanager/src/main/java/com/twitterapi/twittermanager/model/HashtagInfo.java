package com.twitterapi.twittermanager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hashtag_info")
@Data
public class HashtagInfo {
    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    private String name;

    private Integer times;
}
