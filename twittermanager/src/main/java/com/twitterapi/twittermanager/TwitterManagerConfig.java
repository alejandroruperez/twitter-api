package com.twitterapi.twittermanager;

import com.twitterapi.twittermanager.services.TwitterStatusListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import twitter4j.FilterQuery;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

@Configuration
public class TwitterManagerConfig {

    @Autowired
    private TwitterStatusListener twitterStatusListener;

    @Bean
    public TwitterStream twitterStream() {
        TwitterStream twitterStream = new TwitterStreamFactory().getInstance();

        twitterStream.addListener(twitterStatusListener);

        FilterQuery filterQuery = new FilterQuery()
                .language("es", "fr", "it")
                .locations(new double[][]{{-180, -90}, {180, 90}});

        twitterStream.filter(filterQuery);

        return twitterStream;
    }
}
