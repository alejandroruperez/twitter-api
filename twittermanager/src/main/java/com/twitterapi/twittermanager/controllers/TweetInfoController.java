package com.twitterapi.twittermanager.controllers;

import com.twitterapi.twittermanager.model.HashtagInfo;
import com.twitterapi.twittermanager.model.TweetInfo;
import com.twitterapi.twittermanager.services.HashtagService;
import com.twitterapi.twittermanager.services.TweetInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class TweetInfoController {

    private final TweetInfoService tweetInfoService;
    private final HashtagService hashtagService;

    @Autowired
    public TweetInfoController(TweetInfoService tweetInfoService, HashtagService hashtagService) {
        this.tweetInfoService = tweetInfoService;
        this.hashtagService = hashtagService;
    }

    @GetMapping("/tweets")
    public List<TweetInfo> getTweets() {
        return tweetInfoService.findAllTweets();
    }

    @PostMapping("/tweet/{tweetId}")
    public void markTweetAsValidated(@PathVariable Long tweetId) {
        Optional<TweetInfo> tweetInfo = tweetInfoService.findTweet(tweetId);

        if(!tweetInfo.isPresent()) {
            return;
        }

        TweetInfo tweet = tweetInfo.get();
        tweet.setValidated(true);

        tweetInfoService.save(tweet);
    }

    @GetMapping("/tweets/validated")
    public List<TweetInfo> getValidatedTweets() {
        return tweetInfoService.findValidatedTweets();
    }

    @GetMapping("/hashtags")
    public List<HashtagInfo> getTop10Hashtags() {
        return hashtagService.getTop10Hashtags();
    }
}
