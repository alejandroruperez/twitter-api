var express = require('express');
var uuidv4 = require('uuid/v4');
var proxy = require('express-http-proxy');
var router = express.Router();

var tokens = [];
var TWITTER_MANAGER_URL = 'twittermanager:8080'

router.post('/login', function(req, res, next) {
    var user = req.body.user;
    var pass = req.body.pass;
    var tokenString = uuidv4();

    tokens.push(tokenString);

    console.log(tokens);

    res.end(JSON.stringify({ token: tokenString }));
});

router.use(function timeLog(req, res, next) {
    var token = req.header('token');
    var isTokenPresent = tokens.indexOf(token) != -1

    if(!token) {
        res.status(401).send('The request does not have any token');
    }

    if(!isTokenPresent) {
        res.status(403).send('The token is not valid');
    }

    next();
});

router.all('/twittermanager/*', function(req,res,next) {
    next();
}, proxy(TWITTER_MANAGER_URL, {
    proxyReqPathResolver: function(req) {
        var newUrl = req.url.replace(/\/twittermanager/, '');
        console.log('Redirecting to ', newUrl);

        return newUrl;
    }, proxyErrorHandler: function(err, res, next) {
        console.error(err);
        next(err);
    }
}));

module.exports = router;
