# Twitter api consumer
The application has been dockerized in order to ease its use.

## Run the application
If you want to run the application, just type:

```
docker-compose up -d
```

And to shut it down, type:

```
docker-compose down
```

## Endpoints
The base URL is *localhost:3000*. The enpoints that are used in the program are:

### POST /login
Perform this login with any given user and password, so the rest of endpoints can be accessed. This is the first endpoint that the user must hit 

### GET /twittermanager/tweets
Show all tweets that have been read from twitter into the local database

### POST /twittermanager/tweet/{id}
Marks the twitter with the specified id as validated

### GET /twittermanager/tweets/validated
Gets all the tweets that have been validated by the user

### GET /twittermanager/hashtags
Gets the information of the most common hashtags in the tweets that have been loaded
